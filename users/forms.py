
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model  = CustomUser
        #fields = UserCreationForm.Meta.fields + ('age',)     # default manner
        fields = ('username', 'email', 'age',)                # preferable manner
 

class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm):
        model  = CustomUser
        #fields = UserChangeForm.Meta.fields 
        fields = ('username', 'email', 'age',)



''' Why i maded changes in  fields ?

The Python programming community agrees that “explicit is better than implicit” so
naming our fields in this fashion is a good idea.
'''