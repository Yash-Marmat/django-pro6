-this project is the continution of the project 5.
-with our previous custom model we are now using authentication features.
-features like login, logout and sign in.

-Important point about templates

By default the Django template loader looks for templates in a nested structure
within each app. So a home.html template in users would need to be located at
users/templates/users/home.html. But a project-level templates folder approach is
cleaner and scales better so that’s what we’ll use.

here i created a new templates directory and within it a registration folder as that’s
where Django will look for the log in template.